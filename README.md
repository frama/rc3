# FraMa Workadventure for rc3


# File & Folders
 
* frama.tiled-project  Project file for the tiled editor
* main.tmx Main Map for rc3
* *.tmx Tiled Map files
* tiles/
* tiles/*.tsx Tiled Tilesets  
* tools/
* external/ Dependencies aka other peoples stuff
* build.sh Export Tiles and copy dependencies to dist/
* dist/ 2bedeployed  
